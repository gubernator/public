with b as (select dl.product, count(dl.*) as b from dsacrm.dwhinform.f_deal as dl
		   --where dl.date_lst_mntd = '31.12.2999' -- Уточнение №1
		   group by dl.product) --Все договора по продуктам
, c as (select dl.product, count(dl.*) as b from dsacrm.dwhinform.f_deal as dl
		   where d1.deal_status ='A'
           --and dl.date_lst_mntd = '31.12.2999' -- Уточнение №1
		   group by dl.product) --Все договора по продуктам в статусе договора 'A'
, d as (select dl.product, sum(d1.principal_value) as osum, sum(d1.ovrdue_principal) as psum from dsacrm.dwhinform.f_deal as dl
		   where d1.deal_status ='A'
           and dl.date_lst_mntd = '31.12.2999'
		   group by dl.product)
select a.category_name, sum(b.b), sum(c.b), sum(d.osum), sum(d.psum) from u_m05k1.rrss_products as a
    LEFT JOIN b ON b.product = a.type --Если предположить, что могут быть продукты по которым нет сделок
    LEFT JOIN c ON c.product = a.type
    LEFT JOIN d ON d.product = a.type
GROUP BY a.category_name

--a.category_name а). название проекта (поле category_name);
--b.b             б). количество всех договоров по каждому проекту;
--c.b             в). количество всех договоров в статусе договора 'A' по каждому проекту;
--d.osum          г). общий портфель по каждому проекту;
--d.psum          д). просроченный портфель по каждому проекту.

/*
Уточнение №1 Если выбирать не актуальные, то будут дубли по количеству договоров ?
Возможно, по договорам есть пролонгация, старый договор закрывается (датой окончания = дата закрытия), 
появляется продлённый с датой окончания + бесконечность. Т.е. У нас может быть так ?(deal_id и date_lst_mntd -> составной ключ):
----------------
product|deal_id|date_lst_mntd|deal_status       |...
ABS    |   1   | '31.12.2999'|    A     (Active)|     
ABS    |   1   | '26.06.2019'|    C     (Clouse)|  
ABS    |   2   | '31.12.2999'|    A             |
*/