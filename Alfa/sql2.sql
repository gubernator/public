SELECT c.client_mnem, 
c.addr_postcode||', '||c.addr_cntry_cod||', '||c.addr_region_type||', г.'||c.addr_city||', '||c.addr_settl_name||', '||c.addr_street_type||' '||c.addr_street_name||', д. '||c.addr_house|| ', корп./стр. '||c.addr_build||', кв. '||c.addr_appt as Adress
            FROM u_m05k1.tst_d_addr_client c            
            join (SELECT 
                b.client_mnem, --идентификатор клиента
                max(b.addr_lst_dt) as addr_lst_dt
                FROM u_m05k1.tst_d_addr_client b
                WHERE (1=1) 
                AND addr_type = 'F'
                AND addr_usr = 'EQ'
                GROUP BY client_mnem) a
                on c.client_mnem = a.client_mnem and c.addr_lst_dt = a.addr_lst_dt  